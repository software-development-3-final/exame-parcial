# README - Reddit API

Este projeto é uma aplicação Spring Boot que implementa uma API para gerenciamento de usuários dependendo de um sistema legado. Ele fornece endpoints para operações CRUD (Create, Read, Update, Delete) em usuários.

## 🚀 Funcionalidades

- **Criar Usuário:** Endpoint para criar um novo usuário.
- **Listar Usuários:** Endpoint para obter a lista de todos os usuários cadastrados.
- **Obter Usuário por ID:** Endpoint para obter os detalhes de um usuário específico com base em seu ID.
- **Atualizar Usuário:** Endpoint para atualizar as informações de um usuário existente.
- **Excluir Usuário:** Endpoint para excluir um usuário existente.

## 📁 Estrutura do Projeto

O projeto possui a seguinte estrutura de pacotes:

- **`controller`:** Contém os controladores responsáveis por receber as requisições HTTP e encaminhá-las para o serviço correspondente.
- **`model`:** Contém as classes de modelo, incluindo o modelo de usuário e um deserializador personalizado para converter JSON em objetos de usuário.
- **`service`:** Contém os serviços responsáveis pela lógica de negócios, como criar, listar, atualizar e excluir usuários.
- **`utils`:** Contém classes utilitárias, como configurações do Jackson ObjectMapper e configurações de serviços legados.

## ⚙️ Configuração

- **Banco de Dados:** O projeto utiliza MySQL como banco de dados. As configurações de conexão estão presentes no arquivo `docker-compose.yml`.
- **Dependências:** O arquivo `pom.xml` contém todas as dependências necessárias para o projeto. Certifique-se de que elas estejam configuradas corretamente.

## 🔌 Integração com Sistema Legado:

Este projeto integra-se com um sistema legado chamado UsersCLI fornecido pela jala, que está disponível como uma dependência no diretório `lib`. O sistema legado é utilizado para determinadas operações de negócio específicas. Certifique-se de manter o arquivo JAR do UsersCLI no diretório `lib`.

## ▶️ Executando o Projeto

Para executar o projeto localmente, siga estas etapas:

1. Clone este repositório.
2. Certifique-se de ter o Docker instalado em sua máquina.
3. Execute a classe principal `RedditApiApplication` para iniciar a aplicação.

Ao ser iniciado pela primeira vez, o Docker será automaticamente configurado para criar e configurar o banco de dados localizado no script init.sql.

Certifique-se de que todas as dependências do projeto estejam instaladas corretamente antes de iniciar a aplicação.

A aplicação estará disponível em `http://localhost:8080`.

## 📖 Documentação da API

A documentação da API é gerada automaticamente usando o SpringDoc OpenAPI (swagger). Após iniciar a aplicação, você pode acessar a documentação em `http://localhost:8080/swagger-ui.html`.


![swagger-doc](reddit-api/assets/img.png)

