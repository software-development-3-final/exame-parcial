CREATE TABLE `users`
(
    `id`       VARCHAR(36)  NOT NULL,
    `name`     VARCHAR(200) NOT NULL,
    `login`    VARCHAR(20)  NOT NULL,
    `password` VARCHAR(100) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE
);


