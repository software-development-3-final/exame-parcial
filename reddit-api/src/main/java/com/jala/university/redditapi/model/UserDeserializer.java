package com.jala.university.redditapi.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import jalau.cis.models.User;

import java.io.IOException;

public class UserDeserializer extends StdDeserializer<User> {

    public UserDeserializer() {
        this(null);
    }

    public UserDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public User deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        JsonNode node = jp.getCodec().readTree(jp);
        String id = node.has("id") ? node.get("id").asText() : null;
        String name = node.has("name") ? node.get("name").asText() : null;
        String login = node.has("login") ? node.get("login").asText() : null;
        String password = node.has("password") ? node.get("password").asText() : null;
        return new User(id, name, login, password);
    }
}
