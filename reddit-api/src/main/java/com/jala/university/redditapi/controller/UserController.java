package com.jala.university.redditapi.controller;

import com.jala.university.redditapi.model.DTO.user.UserResponseDTO;
import com.jala.university.redditapi.service.UserManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import jalau.cis.models.User;
import com.jala.university.redditapi.model.DTO.user.UserRequestDTO;


import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserManagementService userManagementService;

    @Autowired
    public UserController(UserManagementService userManagementService) {
        this.userManagementService = userManagementService;
    }
    @PostMapping
    public ResponseEntity<String> createUser(@RequestBody UserRequestDTO userRequestDTO) {
        try {
            String userId = generateUserId();
            User user = new User(userId, userRequestDTO.getName(), userRequestDTO.getLogin(), userRequestDTO.getPassword());
            userManagementService.createUser(user);
            return new ResponseEntity<>("User created successfully with ID: " + userId, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>("Failed to create user: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private String generateUserId() {
        return UUID.randomUUID().toString();
    }

    @GetMapping
    public ResponseEntity<List<UserResponseDTO>> getAllUsers() {
        try {
            List<User> users = userManagementService.getAllUsers();
            List<UserResponseDTO> userResponseDTOS = new ArrayList<>();
            for (User user : users) {
                UserResponseDTO userResponseDTO = new UserResponseDTO(user.getId(), user.getName(), user.getLogin());
                userResponseDTOS.add(userResponseDTO);
            }
            return new ResponseEntity<>(userResponseDTOS, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @GetMapping("/{userId}")
    public ResponseEntity<UserResponseDTO> getUserById(@PathVariable String userId) {
        try {
            User user = userManagementService.getUserById(userId);
            if (user != null) {
                UserResponseDTO userResponseDTO = new UserResponseDTO(user.getId(), user.getName(), user.getLogin());
                return ResponseEntity.ok(userResponseDTO);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PutMapping("/{userId}")
    public ResponseEntity<String> updateUser(@PathVariable String userId, @RequestBody UserRequestDTO userRequestDTO) {
        try {
            User user = new User(userId, userRequestDTO.getName(), userRequestDTO.getLogin(), userRequestDTO.getPassword());
            userManagementService.updateUser(user);
            return new ResponseEntity<>("User updated successfully", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Failed to update user: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @DeleteMapping("/{userId}")
    public ResponseEntity<String> deleteUser(@PathVariable String userId) {
        try {
            userManagementService.deleteUser(userId);
            return new ResponseEntity<>("User deleted successfully", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Failed to delete user: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
