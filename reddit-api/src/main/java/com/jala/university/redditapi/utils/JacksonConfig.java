package com.jala.university.redditapi.utils;

import com.fasterxml.jackson.databind.module.SimpleModule;
import com.jala.university.redditapi.model.UserDeserializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.fasterxml.jackson.databind.ObjectMapper;
import jalau.cis.models.User;

@Configuration
public class JacksonConfig {

    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(User.class, new UserDeserializer());
        objectMapper.registerModule(module);
        return objectMapper;
    }
}
