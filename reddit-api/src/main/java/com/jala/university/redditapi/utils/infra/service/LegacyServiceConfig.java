package com.jala.university.redditapi.utils.infra.service;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import jalau.cis.services.UsersService;
import jalau.cis.services.ServicesFacade;

@Configuration
public class LegacyServiceConfig {

    @Bean
    public UsersService usersService() throws Exception {
        ServicesFacade servicesFacade = ServicesFacade.getInstance();
        servicesFacade.init("lib/sd3.xml");
        return servicesFacade.getUsersService();
    }
}
