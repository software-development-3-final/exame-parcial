package com.jala.university.redditapi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jalau.cis.models.User;
import jalau.cis.services.UsersService;

import java.util.List;

@Service
public class UserManagementService {

    private final UsersService usersService;

    @Autowired
    public UserManagementService(UsersService usersService) {
        this.usersService = usersService;
    }

    public void createUser(User user) throws Exception {
        usersService.createUser(user);
    }

    public List<User> getAllUsers() throws Exception {
        return usersService.getUsers();
    }

    public void updateUser(User user) throws Exception {
        usersService.updateUser(user);
    }

    public void deleteUser(String userId) throws Exception {
        usersService.deleteUser(userId);
    }

    public User getUserById(String userId) throws Exception {
        List<User> users = usersService.getUsers();
        for (User user : users) {
            if (user.getId().equals(userId)) {
                return user;
            }
        }
        return null;
    }

}
