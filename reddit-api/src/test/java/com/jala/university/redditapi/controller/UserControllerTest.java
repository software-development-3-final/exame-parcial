package com.jala.university.redditapi.controller;

import com.jala.university.redditapi.model.DTO.user.UserRequestDTO;

import com.jala.university.redditapi.service.UserManagementService;
import jalau.cis.models.User;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

@WebMvcTest(UserController.class)
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserManagementService userManagementService;

    @Test
    public void testGetAllUsers() throws Exception {
        User user1 = new User("1", "John", "john@example.com", "password");
        User user2 = new User("2", "Alice", "alice@example.com", "password");

        List<User> userList = new ArrayList<>();
        userList.add(user1);
        userList.add(user2);

        when(userManagementService.getAllUsers()).thenReturn(userList);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/users"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(user1.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(user1.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].login").value(user1.getLogin()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id").value(user2.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name").value(user2.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].login").value(user2.getLogin()));

        verify(userManagementService, times(1)).getAllUsers();
    }

    @Test
    public void testGetUserById() throws Exception {
        User user = new User("1", "John", "john@example.com", "password");

        when(userManagementService.getUserById("1")).thenReturn(user);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(user.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(user.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.login").value(user.getLogin()));

        verify(userManagementService, times(1)).getUserById("1");
    }

    @Test
    public void testUpdateUser() throws Exception {
        UserRequestDTO userRequestDTO = new UserRequestDTO("John", "john@example.com", "password");
        User user = new User("1", userRequestDTO.getName(), userRequestDTO.getLogin(), userRequestDTO.getPassword());

        mockMvc.perform(MockMvcRequestBuilders.put("/api/users/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\": \"John\",\"login\": \"john@example.com\",\"password\": \"password\"}"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("User updated successfully"));

        verify(userManagementService, times(1)).updateUser(any(User.class));
    }

    @Test
    public void testDeleteUser() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/users/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("User deleted successfully"));

        verify(userManagementService, times(1)).deleteUser("1");

    }
}

